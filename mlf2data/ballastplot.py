#!/usr/bin/env python
#
# $Id: ballastplot.py,v 84da44496c51 2008/07/29 18:58:17 mikek $
#
"""
 %prog [options] envfile

Create a ballasting summary plot from an MLF2 'env' data file.
"""
from mlf2data import netcdf, plotting
import pylab
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg
from optparse import OptionParser

def main():
    op = OptionParser(usage=__doc__)
    op.add_option("-s", "--size",
                  type="string",
                  default="8x6",
                  dest="size",
                  metavar="WIDTHxHEIGHT",
                  help="figure size in inches")
    op.add_option("-f", "--fontsize",
                  type="int",
                  default=10,
                  dest="fontsize",
                  help="font size in points")
    op.add_option("-i", "--image",
                  type="string",
                  default=None,
                  dest="image",
                  metavar="FILENAME",
                  help="output plot as an image file")
    
    opts, args = op.parse_args()
    if len(args) != 1:
        op.error('Need to specify input file')

    width, height = [int(x) for x in opts.size.split('x')]

    if opts.image:
        fig = Figure(figsize=(width, height))
        plotting.summary_plot(args[0], fig, opts.fontsize)
        canvas = FigureCanvasAgg(fig)
        canvas.print_figure(opts.image, dpi=96)
    else:
        fig = pylab.figure(1, figsize=(width, height))
        plotting.summary_plot(args[0], fig, opts.fontsize)
        pylab.show()
    
if __name__ == '__main__':
    main()
