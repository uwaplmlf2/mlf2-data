#!/usr/bin/env python
#
"""
.. module:: suna
   :synopsis: parse Satlantic SUNA binary data records
"""
from construct import Struct, BFloat32, UBInt32, String,\
    UBInt16, UBInt8, Array, ConstructError, SBInt32, Rename


SunaRecord = Struct("SunaRecord",
                    String("header", 6),
                    String("sn", 4),
                    BFloat32("time"),
                    BFloat32("nitrate_um"),
                    BFloat32("nitrate_mg"),
                    BFloat32("nitrate_error"),
                    BFloat32("t_lamp"),
                    BFloat32("t_spec"),
                    UBInt32("lamp_time"),
                    BFloat32("humidity"),
                    BFloat32("volt_12"),
                    BFloat32("volt_reg"),
                    BFloat32("volt_main"),
                    UBInt16("spec_avg"),
                    UBInt16("dark_avg"),
                    Array(226, UBInt16("channel")),
                    UBInt8("checksum"))

DataRecord = Struct("DataRecord",
                    SBInt32("timestamp"),
                    Rename("data", SunaRecord))


def read_stream(infile, oldstyle=False):
    """
    Iterate over all of the SUNA data records in a file-like object.
    """
    obj = SunaRecord if oldstyle else DataRecord
    try:
        rec = obj.parse_stream(infile)
        while rec:
            yield rec
            rec = obj.parse_stream(infile)
    except (ConstructError, ValueError):
        return


def read_buffer(buf, oldstyle=False):
    """
    Parse a SUNA data record from a binary string or buffer.
    """
    obj = SunaRecord if oldstyle else DataRecord
    try:
        return obj.parse(buf)
    except (ConstructError, ValueError):
        return None
