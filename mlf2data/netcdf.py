#!/usr/bin/env python
#
"""
.. module:: netcdf
   :synopsis: work with MLF2 netCDF data-sets.
"""
import netCDF4
import numpy
import copy
import itertools
import csv

def _common(L1, L2):
    """
    Return the indicies of elements in L2 which also appear
    in L1. Both iterables must be sorted in ascending order.
    """
    indicies = []
    i = 0
    for e in L1:
        try:
            while L2[i] < e:
                i = i + 1
            if e == L2[i]:
                indicies.append(i)
        except IndexError:
            break
    return indicies

class DataSet(dict):
    """
    Class representing a time-series dataset from an MLF2
    netCDF data file.
    """
    def __init__(self, **kwds):
        self._attrs = {}
        self._global = {}
        for name, v in kwds.items():
            self[name] = v

    def __setitem__(self, name, value):
        dict.__setitem__(self, name, numpy.array(value).flatten())

    def __len__(self):
        try:
            return len(self['time'])
        except KeyError:
            return 0

    def __add__(self, dset):
        """Concatenate this DataSet with another and return the result."""
        if self.keys() != dset.keys():
            raise ValueError, 'Cannot concatenate DataSets with different variables'
        result = DataSet()
        result._attrs = copy.deepcopy(self._attrs)
        result._global = copy.deepcopy(self._global)
        for name in self.keys():
            result[name] = numpy.concatenate((self[name], dset[name]))
        return result

    def __iadd__(self, dset):
        """Modify this DataSet by concatenating another."""
        if self.keys() != dset.keys():
            raise ValueError, 'Cannot concatenate DataSets with different variables'
        for name in self.keys():
            self[name] = numpy.concatenate((self[name], dset[name]))
        return self

    def __and__(self, dset):
        """Merge this DataSet with another using the 'time' variable."""
        assert 'time' in self
        assert 'time' in dset
        i1 = _common(self['time'], dset['time'])
        i2 = _common(dset['time'], self['time'])
        if not i1 or not i2:
            raise ValueError, 'Cannot merge DataSets with no common points'
        result = DataSet()
        for name in self.keys():
            result[name] = self[name][i2]
            try:
                result.set_attrs(name, self.get_attrs(name))
            except KeyError:
                pass
        for name in dset.keys():
            if name not in result:
                result[name] = dset[name][i1]
                try:
                    result.set_attrs(name, dset.get_attrs(name))
                except KeyError:
                    pass
        return result
    
    def __iand__(self, dset):
        """Modify this DataSet by merging with another using the 'time' variable."""
        assert 'time' in self
        assert 'time' in dset
        i1 = _common(self['time'], dset['time'])
        i2 = _common(dset['time'], self['time'])
        if not i1 or not i2:
            raise ValueError, 'Cannot merge DataSets with no common points'
        for name in self.keys():
            self[name] = self[name][i2]
        for name in dset.keys():
            if name not in self:
                self[name] = dset[name][i1]
                try:
                    self.set_attrs(name, dset.get_attrs(name))
                except KeyError:
                    pass
        return self
    
    def get_attrs(self, name):
        """
        Return the attributes for a variable.

        @param name: variable name.
        @return: dictionary of attributes.
        """
        return self._attrs[name]

    def set_attrs(self, name, value):
        """
        Set the attributes for a variable.

        @param name: variable name
        @param value: dictionary of attributes.
        """
        assert name in self
        self._attrs[name] = value
        
    def range(self):
        """
        Time range of this dataset.
        """
        return self['time'][0], self['time'][-1]
    
    def window(self, start, end):
        """
        Extract a subset of the data over a given time range.

        :param start: start time in seconds since the epoch (1/1/1970 UTC)
        :param end: end time in seconds since the epoch
        :return: new :class:`DataSet`
        """
        assert 'time' in self
        result = DataSet()
        result._attrs = copy.deepcopy(self._attrs)
        t1 = self['time'] >= start
        t2 = self['time'] <= end
        indicies = numpy.logical_and(t1, t2).nonzero()
        for name, v in self.items():
            result[name] = v[indicies]
        return result

    def csv(self, outfile):
        """
        Write this DataSet in CSV format.

        :param outfile: output file object.
        """
        names = self.keys()
        names.remove('time')
        names.insert(0, 'time')
        writer = csv.writer(outfile, delimiter=',', quoting=csv.QUOTE_NONE)
        writer.writerow(names)
        columns = [self[name] for name in names]
        for row in itertools.izip(*columns):
            writer.writerow(row)
    
    def applyfunc(self, func, *args):
        """
        Apply a function to DataSet variables.

        :param func: function to call
        :type func: callable
        :param args: list of variable names.
        :return: function result.
        """
        vargs = [self[name] for name in args]
        try:
            vfunc = numpy.vectorize(func, otypes=[numpy.float])
            return vfunc(*vargs)
        except ValueError:
            n = len(self)
            r = numpy.zeros((n,))
            for i in range(n):
                a = [v[i] for v in vargs]
                r[i] = func(*a)
            return r
            

def readfile(filename):
    nc = netCDF4.Dataset(filename, 'r')
    ds = DataSet()
    for name, v in nc.variables.items():
        anames = [aname for aname in dir(v) if not callable(getattr(v, aname))]
        attrs = dict([(aname, getattr(v, aname)) for aname in anames])
        if len(v.dimensions) > 1 and v.shape[1] > 1:
            for i in range(v.shape[1]):
                vname = '%s%d' % (name, i)
                ds[vname] = v[:,i]
                ds.set_attrs(vname, attrs)
        else:
            ds[name] = v[:]
            ds.set_attrs(name, attrs)
    return ds

def writefile(ds, filename):
    typecodes = {'int32' : 'i4', 'float' : 'f4', 'int16' : 'i2'} 
    nc = netCDF4.Dataset(filename, 'w')
    nc.createDimension('time', None)
    names = ds.keys()
    names.remove('time')
    names.insert(0, 'time')
    for name in names:
        ncvar = nc.createVariable(name, typecodes.get(ds[name].dtype.name, 'f'), ('time',))
        if name in ds._attrs:
            for k, v in ds._attrs[name].items():
                try:
                    setattr(ncvar, k, v)
                except AttributeError:
                    pass
        ncvar.assignValue(ds[name])
    nc.close()
            
def tocsv():
    import sys
    try:
        ds = readfile(sys.argv[1])
    except:
        sys.exit(1)
    for name in sys.argv[2:]:
        ds += readfile(name)
    ds.csv(sys.stdout)

def merge():
    import sys
    try:
        ds = readfile(sys.argv[1])
        ds &= readfile(sys.argv[2])
        writefile(ds, sys.argv[3])
    except IndexError as e:
        print str(e)
        sys.stderr.write('Usage: ncmerge file1 file2 outfile\n')

try:
    import pandas as pd

    def to_dataframe(self):
        """
        Convert a DataSet to a :class:`pandas.DataFrame` instance.
        """
        names = self.keys()
        names.remove('time')
        df = pd.DataFrame(dict([(name, self[name]) for name in names]),
                          index=self['time'])
        df.index = pd.to_datetime(df.index, unit='s')
        df.tz_localize('UTC')
        return df

    DataSet.dataframe = to_dataframe
except ImportError:
    pass


if __name__ == '__main__':
    tocsv()
