#!/usr/bin/env python
#
# $Id: plotting.py,v 695e5d3d2a88 2008/09/03 19:41:39 mikek $
#
import pytz
import math
import gsw
from matplotlib.dates import date2num, HourLocator, MinuteLocator,\
    DateFormatter
from matplotlib.ticker import NullFormatter
from datetime import datetime
from mlf2data import netcdf


def sigma_t(s, t):
    return gsw.rho(s, t, 0) - 1000.


def twinx(ax):
    ax2 = ax.figure.add_axes(ax.get_position(), sharex=ax, frameon=False)
    ax2.yaxis.set_label_position('right')
    ax.yaxis.tick_left()
    return ax2


def plot_ballast(ax, t, depth, disp, xlabel='', fontsize=10, **kwds):
    """
    Create a plot showing depth vs. time and piston displacement vs. time.

    @param ax: axes object for the plot
    @type ax: C{matplotlib.axes}
    @param t: time record
    @type t: C{numpy.array}
    @param depth: depth record (decibars)
    @type depth: C{numpy.array}
    @param disp: displacement record (cc)
    @type disp: C{numpy.array}
    @param xlabel: label for the time (x) axis.
    @param fontsize: fontsize for axes labels.
    """
    tz = pytz.utc
    l1, = ax.plot_date(t, depth, fmt='r.', label='depth', tz=tz)
    ax.set_ylabel('-dbars', fontsize=fontsize)

    try:
        ax = ax.twinx()
    except AttributeError:
        ax = twinx(ax)

    l2, = ax.plot_date(t, disp, fmt='g.', tz=tz, label='displacement')
    ax.yaxis.tick_right()
    ax.set_ylabel('cc', fontsize=fontsize)

    for option, value in kwds.items():
        func = getattr(ax.xaxis, 'set_'+option)
        if func:
            func(value)

    for label in ax.get_yticklabels():
        label.set_fontsize(fontsize)

    if xlabel:
        ax.set_xlabel(xlabel, fontsize=fontsize)
        for label in ax.get_xticklabels():
            label.set_fontsize(fontsize)
    else:
        ax.set_xticklabels([])

    ax.legend((l1, l2), ('depth', 'displacement'), loc=0)


def plot_density(ax, t, dens, xlabel='', fontsize=10, **kwds):
    """
    Create a plot showing water density vs. time

    @param ax: axes object for the plot
    @type ax: C{matplotlib.axes}
    @param t: time record
    @type t: C{numpy.array}
    @param dens: density record (sigma units)
    @type dens: C{numpy.array}
    @param xlabel: label for the time (x) axis.
    @param fontsize: fontsize for axes labels.
    """
    ax.plot_date(t, dens, fmt='b.', tz=pytz.utc, label='density')

    for option, value in kwds.items():
        func = getattr(ax.xaxis, 'set_'+option)
        if func:
            func(value)

    ax.legend(loc=0)
    ax.set_ylabel(r'$-\sigma_T$', fontsize=fontsize)
    for label in ax.get_yticklabels():
        label.set_fontsize(fontsize)
    if xlabel:
        ax.set_xlabel(xlabel, fontsize=fontsize)
        for label in ax.get_xticklabels():
            label.set_fontsize(fontsize)
    else:
        ax.set_xticklabels([])


def summary_plot(datafile, fig, fontsize):
    """
    Create a summary plot figure from an MLF2 'env' data file. The
    figure will contain two plots, one showing the depth and piston
    displacement vs. time and the other showing water density vs. time.

    @param datafile: name of env data file.
    @param fig: matplotlib figure instance.
    @param fontsize: font size for plot text (points)
    """
    data = netcdf.readfile(datafile)
    if 'sal0' in data.keys():
        dens = data.applyfunc(sigma_t, 'sal0', 'temp0')
    else:
        dens = data.applyfunc(sigma_t, 'sal', 'temp')
    dia = 5.072
    scale = math.pi*dia*dia/4.
    disp = data['piston'] * scale
    t = [date2num(datetime.utcfromtimestamp(t)) for t in data['time']]

    dt = t[-1] - t[0]
    if dt <= 0.5:
        major = HourLocator(tz=pytz.utc)
        minor = MinuteLocator(interval=15, tz=pytz.utc)
    elif dt < 1.25:
        major = HourLocator(byhour=range(0, 24, 4), tz=pytz.utc)
        minor = HourLocator(byhour=range(24), tz=pytz.utc)
    else:
        major = HourLocator(byhour=[0, 12], tz=pytz.utc)
        minor = HourLocator(byhour=range(0, 24, 4), tz=pytz.utc)

    # format the x-axis
    hoursFmt = DateFormatter('%j/%H')
    ax = fig.add_subplot(211)
    ax.grid(True)

    if 'pressure0' in data.keys():
        pr = -data['pressure0']
    else:
        pr = -data['pressure']
    plot_ballast(ax, t, pr, disp, fontsize=fontsize,
                 major_locator=major,
                 minor_locator=minor, major_formatter=hoursFmt,
                 minor_formatter=NullFormatter())

    ax = fig.add_subplot(212)
    ax.grid(True)

    plot_density(ax, t, -dens, xlabel='time [yday/hour UTC]',
                 fontsize=fontsize,
                 major_locator=major,
                 minor_locator=minor, major_formatter=hoursFmt,
                 minor_formatter=NullFormatter())
