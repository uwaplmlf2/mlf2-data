#!/usr/bin/env python
#
# $Id: tesac.py,v 7dfc612f8a90 2008/08/08 18:24:28 mikek $
#
# Functions to create a TESAC_ report from an MLF2 profile.
#
# .. _TESAC: http://www.meds-sdmm.dfo-mpo.gc.ca/meds/Prog_Int/J-COMM/CODES/tesaccode_e.htm
#
import textwrap
from datetime import datetime

# Line terminator within the TESAC report
eol = '\r\n'

# WMO Data Designator
#  S := surface data
#  O := oceanographic data
#  V := mobile station
#  X := multiple geographic area
# 10 := global distribution
data_designator = 'SOVX10'

# WMO code for originating station
station_code = 'KWBC'

class LocationError(Exception):
    def __init__(self, lat, lon):
        self.lat, self.lon = lat, lon

    def __str__(self):
        return 'invalid location %s/%s' % (str(self.lat), str(self.lon))

def tesac_profile(depth, temperature, salinity, ptype='down'):
    """
    Encode the temperature and salinity data.

    >>> tesac_profile([1.1, 3.6, 2.2], [-9.25, 9.67, 9.5], [28.2, 29.1, 28.4])
    ['88871', '85665', '20001', '35925', '42820', '20002', '30950', '42840', '20004', '30967', '42910']
    >>> tesac_profile([1.1, 2.2, 2.2], [-9.25, 9.67, 9.5], [28.2, 29.1, 28.4])
    ['88871', '85665', '20001', '35925', '42820', '20002', '30950', '42840']
    
    @param depth: iterable of depth values in meters
    @param temperature: iterable of temperature values in degrees C
    @param salinity: iterable of salinity values in psu
    @param ptype: profile type, 'up' or 'down'
    @return: C{list} of strings encoding the profile.
    """
    values = ['88871']
    if ptype == 'down':
        values.append('85665')
    else:
        values.append('85664')
    profile = []
    for z, t, s in zip(depth, temperature, salinity):
        if z < 0 or s < 0:
            continue
        # Add 50 to all temperatures < 0
        if t < 0:
            t = abs(t) + 50
        profile.append(('2%04d' % int(z+0.5),
                        '3%04d' % int(t*100+0.5),
                        '4%04d' % int(s*100+0.5)))
    # Depths need to be in descending order and each
    # depth may only appear once.
    profile.sort()
    d = {}
    for z, t, s in profile:
        if z not in d:
            values.extend([z, t, s])
            d[z] = 1
    return values

def tesac_header(timestamp, lat, lon):
    """
    Encode 'Section 1' TESAC metadata.

    >>> from datetime import datetime
    >>> tesac_header(datetime(2008, 1, 1, 13, 0, 0), 47.74063, -122.40649)
    ['KKYY', '01018', '1300/', '747741', '122406']
    
    @param timestamp: profile time
    @type timestamp: C{datetime.datetime}
    @param lat: latitude in degrees
    @param lon: longitude in degrees.
    @return: C{list} of strings encoding the header.
    """
    quadrant = 0
    if lat >= 0 and lon >= 0:
        quadrant = 1
    elif lat < 0 and lon >= 0:
        quadrant = 3
    elif lat < 0 and lon < 0:
        quadrant = 5
    elif lat >= 0 and lon < 0:
        quadrant = 7
        
    try:
        assert quadrant > 0
        ilat = int(abs(lat)*1000 + 0.5)
        ilon = int(abs(lon)*1000 + 0.5)
    except:
        raise LocationError, (lat, lon)
    values = ['KKYY']
    values.append('%02d%02d%1d' % (timestamp.day, timestamp.month,
                                  timestamp.year%10))
    values.append('%02d%02d/' % (timestamp.hour, timestamp.minute))
    values.append('%1d%05d' % (quadrant, ilat))
    values.append('%06d' % ilon)
    return values

def tesac_report(wmo_id, header, profile, columns=50):
    """
    Combine the header and profile into a TESAC report.

    @param wmo_id: WMO ID code for this float
    @type wmo_id: C{string}
    @param header: TESAC section 1.
    @type header: C{list} of strings
    @param profile: TESAC encoded profile
    @type profile: C{list} of strings
    @param columns: maximum number of columns in the output.
    @return: C{list} of strings, one per line.
    """
    if len(wmo_id) > 5:
        trailer = 'Q%s=' % wmo_id
    else:
        trailer = '99999 %s=' % wmo_id
    contents = [' '.join(header), ' '.join(profile), trailer]
    return textwrap.wrap(' '.join(contents), width=columns)

def gts_enclosure(report, seq):
    """
    Wrap the output from tesac_report in a GTS 'enclosure'.

    @param report: output from L{tesac_report}
    @type report: C{list} of C{strings}
    @param seq: message sequence number (1-999)
    @type seq: C{int}
    @return: GTS message contents
    @rtype: C{string}
    """
    gts_eol = '\r\r\n'
    seq = seq % 1000
    if seq == 0:
        seq = 1
    som = '\x01'
    eom = '\x03'
    # WMO Abbreviated Heading.
    tstamp = datetime.utcnow().strftime('%d%H%M')
    heading = ' '.join([data_designator, station_code, tstamp])
    contents = gts_eol.join([som, '%03d' % seq, heading] + report + [eom])
    return contents

def ftp_enclosure(report):
    """
    Wrap the output from tesac_report in an FTP 'enclosure'.

    @param report: output from L{tesac_report}
    @type report: C{list} of C{strings}
    @return: FTP file contents
    @rtype: C{string}
    """
    # Prepend the WMO Abbreviated Heading to the report.
    tstamp = datetime.utcnow().strftime('%d%H%M')
    heading = ' '.join([data_designator, station_code, tstamp])
    contents = eol.join([heading] + report)
    
    size = len(contents)
    if size > 999999:
        raise ValueError, 'File size exceeds 999999 bytes'
    # WMO Bulletin Flag Field Separator (ref. http://www.weather.gov/tg/fstandrd.html)
    bulletin_flag = '####018%06d####%s' % (size, eol)
    return bulletin_flag + contents

def _test():
    import doctest
    doctest.testmod()

if __name__ == "__main__":
    _test()
    # import sys
    # p = tesac_profile([1.1, 2.2, 3.6], [9.25, 9.5, 9.67], [28.2, 28.4, 29.1])
    # hdr = tesac_header(datetime(2008, 1, 1, 13, 0, 0), 47.74063, -122.40649)
    # output = tesac_report('41708', hdr, p)
    # sys.stdout.write(gts_enclosure(output, 1234))
    # sys.stdout.write(ftp_enclosure(output))
