#!/usr/bin/env python
#
# $Id: eng.py,v f5dd3b2c69f9 2008/07/25 22:51:42 mikek $
#
# Read an MLF2 engineering file.
#
import csv

MODES = {0 : 'down', 1 : 'settle',
         2 : 'up', 3 : 'driftiso',
         4 : 'driftseek', 5 : 'driftml',
         9 : 'done', 10 : 'start'}

def _transitions(filename):
    """Locate sampling-mode transitions"""
    reader = csv.DictReader(open(filename, 'r'))
    last_mode = -1
    for line in reader:
        mode = int(line['mode'])
        if last_mode != mode:
            yield int(line['time']), last_mode, mode
            last_mode = mode
    yield int(line['time']), mode, -1
    
def find_profiles(filename):
    """
    Read an engineering file and return a list of
    tuples describing each profile:

      type, start, end

    Where type is 'up' or 'down'
    """
    start = 0
    profile_modes = {0 : 'down', 2 : 'up'}
    for t, mode1, mode2 in _transitions(filename):
        if mode1 in profile_modes:
            yield profile_modes[mode1], start, t
        if mode2 in profile_modes:
            start = t

def all_modes(filename):
    start = 0
    for t, mode1, mode2 in _transitions(filename):
        try:
            yield MODES[mode1], start, t
        except KeyError:
            pass
        start = t

            
if __name__ == '__main__':
    import sys
    for p in all_modes(sys.argv[1]):
        print p
        
